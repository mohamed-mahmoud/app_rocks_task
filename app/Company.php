<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;
use App\Image;
class Company extends Model
{
    protected $table="companies";
    protected $fillable=['name','email','website'];

    public function sections()
    {
        return $this->hasMany('App\Employee','company_id');
    }

    public function image()
    {
        return $this->hasOne('App\Image','company_id');
    }
}
