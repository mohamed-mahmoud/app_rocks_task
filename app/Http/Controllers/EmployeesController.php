<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Employee;

class EmployeesController extends Controller
{
    public function index()
    {
        $employee = Employee::query()->paginate(10);

        return view('admin.employees.index', compact('employee'));
    }

    public function show($id)
    {
        $company = Company::all();

        $employee = Employee::find($id);
        return view('admin.employees.show', compact('employee','company'));
    }

    public function edit($id)
    {
        $company = Company::all();
        $employee = Employee::find($id);
        return view('admin.employees.edit', compact('employee','company'));
    }

    public function update($id, Request $request)
    {

        $request->validate([
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'email',
        ]);
        $employee = Employee::find($id);
        $employee->update($request->all());

        return redirect('/employee');
    }

    public function insert()
    {
         $company=Company::all();
          $employee = Employee::all();
        return view('admin.employees.insert', compact('employee','company'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'email',
        ]);
        Employee::create($request->all());
        return redirect('/employee');
    }

    public function delete($id)
    {
         DB::table('employees')->where('id', '=', $id)->delete();

        return redirect('/employee');
    }
}
