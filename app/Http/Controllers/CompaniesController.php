<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Image;
use Dotenv\Validator;
use Illuminate\Http\Request;
use DB;
use App\Company;

class CompaniesController extends Controller
{
    public function index()
    {
        $company=Company::paginate(10);
//        $image=DB::table('images')->value('image');

        return view('admin.companies.index',compact('company'));
    }

    public function get_employees($id){

        //  $sections= mainSection::with('sections')->where('id',$id)->first();
        $employees = Employee::query()->where('company_id','=',$id)->get();
        return view('admin.companies.employees',compact('employees'));

    }

    public function show($id)
    {
        $company=Company::find($id);

        return view('admin.companies.show',compact('company'));
    }

    public function edit($id)
    {
        $company=Company::find($id);
        return view('admin.companies.edit',compact('company'));
    }

    public function update($id,Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'email',
            'website'=>'nullable|url',
        ]);
        $company=Company::find($id);
        $company->name=$request->name;
        $company->email=$request->email;
        $company->website=$request->website;
        $company->save();

        DB::table('images')->where('company_id','=',$id)->delete();
        $image=new Image();
        $file = $request->image;
        $path1 = 'companyImages/';
        $fileName2 = time() . ".png";
        $success1 = $file->move($path1, $fileName2);
        $image->image = $fileName2;

        $image->company_id=$company->id;
        $image->save();
        return redirect('/company');
    }

    public function insert()
    {
        return view('admin.companies.insert');
    }

    public function store(Request $request)
    {
        $request->validate([
           'name'=>'required',
           'email'=>'email',
           'website'=>'nullable|url',
        ]);
        $company = Company::create($request->all());
        $image=new Image();
        $file = $request->image;
        $path1 = 'companyImages/';
        $fileName2 = time() . ".png";
        $success1 = $file->move($path1, $fileName2);
        $image->image = $fileName2;

        $image->company_id=$company->id;
        $image->save();
        return redirect('/company');
    }

    public function delete($id)
    {
       DB::table('companies')->where('id','=',$id)->delete();

        return redirect('/company');
    }
}
