<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table="employees";
    protected $fillable=['firstName','lastName','email','phone','company_id'];

    public function company(){
        return $this->hasOne(Company::class);
    }
}
