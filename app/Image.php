<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Company;
class Image extends Model
{
    protected $table="images";
    protected $fillable=['image','company_id'];
    public function companies()
    {
        return $this->hasOne('App\Company','company_id');
    }
}
