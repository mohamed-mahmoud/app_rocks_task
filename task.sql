-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2018 at 11:32 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `email`, `website`, `created_at`, `updated_at`) VALUES
(3, 'Viva Goodwin', 'mturner@example.net', 'http://www.yundt.com/', '2018-09-30 22:38:17', '2018-09-30 22:38:17'),
(4, 'Karlie Rohan', 'michel.pacocha@example.org', 'http://russel.info/', '2018-09-30 22:38:17', '2018-09-30 22:38:17'),
(5, 'Alison Harber', 'coby.nikolaus@example.net', 'http://stamm.com/mollitia-tenetur-et-et-distinctio', '2018-09-30 22:38:17', '2018-09-30 22:38:17'),
(6, 'Leila Effertz', 'mariela44@example.net', 'https://www.schaefer.biz/sint-dignissimos-deleniti-voluptas-soluta-voluptatem', '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(7, 'Sandy Feeney II', 'luciano.runolfsdottir@example.net', 'http://auer.net/non-eius-consectetur-sunt-qui-doloribus.html', '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(8, 'Frank Ziemann', 'wilton45@example.org', 'http://murphy.biz/', '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(9, 'Dr. Erika Torphy', 'marianne15@example.org', 'http://www.rowe.net/omnis-est-praesentium-eos-aut-repellendus-unde-voluptas', '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(10, 'Prof. Cheyenne Luettgen', 'dennis78@example.com', 'http://windler.com/voluptas-numquam-et-corporis-illo-voluptatem-sed-laudantium', '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(11, 'Prof. John Koepp Jr.', 'yasmine79@example.net', 'https://johnson.com/pariatur-quasi-totam-commodi-veritatis.html', '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(12, 'Kory Schroeder', 'magali.fritsch@example.com', 'http://spencer.com/perferendis-ipsum-molestiae-molestiae-sint-provident-non', '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(13, 'subscriber', 'egypt@yahoo.com', 'https://www.facebook.com/', '2018-10-01 04:25:17', '2018-10-01 04:25:17'),
(14, 'subscriber', 'togetherforever209@yahoo.com', 'https://www.facebook.com/', '2018-10-01 04:26:21', '2018-10-01 04:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `firstName`, `lastName`, `email`, `phone`, `company_id`, `created_at`, `updated_at`) VALUES
(3, 'Demarcus Watsica', 'Maryjane Hintz Jr.', 'ukilback@example.org', '(992) 670-1585 x9525', 7, '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(4, 'Mrs. Estell Zulauf PhD', 'Linnie Johnson', 'felicity46@example.com', '754-295-6542', 6, '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(5, 'Keven Rogahn', 'Rupert Homenick', 'junius.bahringer@example.org', '(984) 888-1827', 9, '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(6, 'Abdul Steuber', 'Marques Kovacek DVM', 'oreilly.alexandra@example.com', '+1-286-767-0157', 3, '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(7, 'Alvah Sipes', 'Maurice Greenholt III', 'jamal81@example.net', '+1.785.360.4921', 4, '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(8, 'Dr. Estel Orn', 'Mrs. Vesta Bergstrom', 'schoen.osborne@example.com', '735.821.5420', 5, '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(9, 'Oceane Lockman', 'Ryleigh Glover Jr.', 'elinor.gutmann@example.com', '+1-805-945-6913', 7, '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(10, 'Mrs. Karianne Brekke', 'Dedric Donnelly', 'magdalena24@example.net', '1-491-940-7085', 8, '2018-09-30 22:38:18', '2018-09-30 22:38:18'),
(11, 'mohamed', 'ali', 'mohamed2020@gmail.com', '77990', 5, '2018-10-01 05:12:29', '2018-10-01 05:12:29'),
(12, 'mohamed', 'ali', 'admin@company.com', '698698', 6, '2018-10-01 05:43:02', '2018-10-01 05:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image`, `company_id`, `updated_at`, `created_at`) VALUES
(2, '8698cebc7951fd016867b34e52d92a26.jpg', 4, '2018-09-30 22:46:07', '2018-09-30 22:46:07'),
(3, '36a2eaf991c0c4f2f001f59c4f928e2f.jpg', 7, '2018-09-30 22:46:07', '2018-09-30 22:46:07'),
(4, 'f9afe0b2a353b16f91570974cc0df2f5.jpg', 4, '2018-09-30 22:46:07', '2018-09-30 22:46:07'),
(5, '063570433071af46a271f76d9367aa0f.jpg', 3, '2018-09-30 22:46:07', '2018-09-30 22:46:07'),
(8, '855fd696d598f710e8ad03075c855216.jpg', 5, '2018-09-30 22:46:07', '2018-09-30 22:46:07'),
(9, 'ba5bfe740e3b6e3905fba20f6416eeae.jpg', 4, '2018-09-30 22:46:07', '2018-09-30 22:46:07'),
(10, '80777987b56ac79f4945c0188039fff3.jpg', 5, '2018-09-30 22:46:07', '2018-09-30 22:46:07'),
(11, '5e1f50da12e381bcc96093aabd781e82.jpg', 3, '2018-09-30 22:46:07', '2018-09-30 22:46:07'),
(12, '1538375117.png', 13, '2018-10-01 04:25:17', '2018-10-01 04:25:17'),
(13, '1538375181.png', 14, '2018-10-01 04:26:21', '2018-10-01 04:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2018_09_27_181526_create_main_section_table', 1),
(3, '2018_09_27_193501_create__section_table', 2),
(8, '2018_09_30_194658_create_companies_table', 3),
(9, '2018_09_30_202148_create_employees_table', 3),
(10, '2018_09_30_211404_create_images_table', 3),
(11, '2014_10_12_100000_create_password_resets_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mohamed', 'mohamed-mahmoud@gmail.com', NULL, '$2y$10$mWcAp3fwnGMmkpSpq7F02OCsyQEk5CxF51OaZkYgMB8CRANw8OzEW', 'Kn7XIiDydh1lMQe6FwKjtF0cXmltb1NbF06KCUB4D9ZD0fh3bwUfdtNHbpDb', '2018-09-30 17:19:14', '2018-09-30 17:19:14'),
(2, 'mohamed', 'admin@company.com', NULL, '$2y$10$xDFRQLkQ7j6e2JbB/mmJdOKCwg50d/WBWP1QfSFRmgw9qC.LUHhJ6', '47iSoiWj7MRGr7c7efyQWk7T5W9hkus90K4A0J2hw8vmGBuYbPIoMax2baBC', NULL, NULL),
(3, 'mohamed', 'admin@company.com', NULL, '$2y$10$P3Vzka/w/ZyNGZqYaep//uFuSCvUIg9tQI4K6egc8WTnAvhPwHp5a', NULL, NULL, NULL),
(4, 'mohamed', 'admin@company.com', NULL, '$2y$10$LcnOE/7rRktRJAdC7cNdde14425xCmEKwQmaxoM3TOMe02QqA95h.', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employees_company_id_foreign` (`company_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_company_id_foreign` (`company_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
