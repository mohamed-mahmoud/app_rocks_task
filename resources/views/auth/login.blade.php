










<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">

    <!-- Viewport Metatag -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <!-- Required Stylesheets -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/bootstrap/css/bootstrap.min.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/fonts/ptsans/stylesheet.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/fonts/icomoon/style.css')}}" media="screen">

    <link rel="stylesheet" type="text/css" href="{{url('assets/css/login.css')}}" media="screen">

    <link rel="stylesheet" type="text/css" href="{{url('assets/css/mws-theme.css')}}" media="screen">

    <title> Login Page</title>

</head>

<body>

<div id="mws-login-wrapper">
    <div id="mws-login">
        <h1>Login</h1>
        <div class="mws-login-lock"><i class="icon-lock"></i></div>
        <div id="mws-login-form">
            <form class="mws-form" action="{{ route('login') }}" method="post">
                {{ csrf_field() }}

                @if (count($errors)>0)
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                @endif
                <div class="mws-form-row">
                    <div class="mws-form-item">
                        <input id="email" type="email"  class="mws-login-username required {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Your_Email" name="email" value="{{ old('email') }}" required autofocus>
                    </div>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="mws-form-row">
                    <div class="mws-form-item">
                        <input type="password" name="password" class="mws-login-password required {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="password">

                    </div>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>

                <div id="mws-login-remember" class="mws-form-row mws-inset">
                    <ul class="mws-form-list inline">
                        <li>
                            <input id="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">Remember me</label>
                        </li>
                    </ul>
                </div>
                <div class="mws-form-row">
                    <input type="submit" value="Login" class="btn btn-success mws-login-button">
                </div>
            </form>
        </div>
    </div>

</div>

<!-- JavaScript Plugins -->
<script src="{{url('assets/js/libs/jquery-1.8.3.min.js')}}"></script>
<script src="{{url('assets/js/libs/jquery.placeholder.min.js')}}"></script>
<script src="{{url('assets/custom-plugins/fileinput.js')}}"></script>

<!-- jQuery-UI Dependent Scripts -->
<script src="{{url('assets/jui/js/jquery-ui-effects.min.js')}}"></script>

<!-- Plugin Scripts -->
<script src="{{url('assets/plugins/validate/jquery.validate-min.js')}}"></script>

<!-- Login Script -->
<script src="{{url('assets/js/core/login.js')}}"></script>

</body>
</html>









{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
