
@extends('admin.master')
@section('content')

    @if (session('messge'))
        <div class="btn-success" style="height:15 px;width: 950px;font-size: 18px;"   >
            {{ session('messge') }}
        </div>
    @endif
    <br>
    <div class="mws-panel grid_8">
        <div class="mws-panel-header">
            <span>Show   {{$employee->firstName}}  Data </span>
        </div>
        <div class="mws-panel-body no-padding">
            <form class="mws-form" action="{{url('/employee/show',$employee->id)}}" method="POST" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="mws-form-inline">

                    <div class="mws-form-row">
                        <label class="mws-form-label">First Name</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$employee->firstName}}"  name="firstName">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <label class="mws-form-label">Last Name</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$employee->lastName}}"  name="lastName">
                        </div>
                    </div>

                    <div class="mws-form-row">
                        <label class="mws-form-label">Email</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$employee->email}}"  name="email">
                        </div>
                    </div>

                    <div class="mws-form-row">
                        <label class="mws-form-label">Phone</label>
                        <div class="mws-form-item">
                            <input type="text" class="small"  value="{{$employee->phone}}"   name="phone">
                        </div>
                    </div>

                    <div class="mws-form-row">
                        <label class="mws-form-label">Company</label>
                        <div class="mws-form-item">
                            <select  class="small" name="company_id"  required>
                                <option></option>
                                @foreach($company as $main )
                                    @if($main->id == $employee->company_id)
                                        <option  value="{{$main->id}}" selected> {{$main->name}} </option>
                                    @else
                                        <option  value="{{$main->id}}"> {{$main->name}} </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
@endsection

