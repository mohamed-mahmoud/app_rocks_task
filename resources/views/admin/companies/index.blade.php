@extends('admin.master')

{{--<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">--}}


@section('content')
    <br>
    <div class="mws-panel-header" style="width: 1030px">
        <button type="button" class="btn btn-secondary" style="font-size: 20px;"><a
                    href="{{url('company/insert')}}">Create New Company</a></button>

    </div><br>
    <div class="mws-panel-body " >
        <table class="mws-datatable-fn mws-table" style="width: 1050px">
            <thead style="width: 1050px">
            <tr style="width: 1050px">

                <th> Name</th>
                <th> Email</th>
                <th> Image</th>

                <th>Employees</th>

                <th>Operations</th>

            </tr>
            </thead>
            @if(isset($company))
                @foreach($company as $value)
                    <tr class="mws-datatable-fn mws-table" style="text-align: center">
                        {{--{{dd($value->image->image)}}--}}
                        <td class="cell100 column1">{!! $value->name !!}</td>
                        <td class="cell100 column1">{!! $value->email !!}</td>

                        <td><img src="companyImages/{{$value->image['image']}}" width="100px" height="100px"></td>

                        <td class="cell100 column1"><a href="{{url('company/getEmployees',$value->id)}}">
                                Employees</a></td>

                        <td>
                            <a href="{{url('company/show',$value->id)}}">
                                <i class="icol-eye" aria-hidden="true"></i> </a>
                            <a href="{{url('company/edit',$value->id)}}">
                                <li class="icol-application-edit"></li>
                            </a>
                            <a href="{{url('company/delete',$value->id)}}"
                               onclick="return confirm('Are you sure you want to delete this item?');">
                                <i class="icol-application-delete" aria-hidden="true">
                                </i></a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>


        {{ $company->links() }}
    </div>
    {{--<br>--}}

@endsection