
@extends('admin.master')
@section('content')

    @if (session('messge'))
        <div class="btn-success" style="height:15 px;width: 950px;font-size: 18px;"   >
            {{ session('messge') }}
        </div>
    @endif
    <br>

    <div class="mws-panel grid_8">

        <div class="mws-panel-body no-padding">
            <form class="mws-form" action="{{url('/company/update',$company->id)}}" method="POST" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="mws-form-inline">

                    <div class="mws-form-row">
                        <label class="mws-form-label">Company Name</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$company->name}}" name="title">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <label class="mws-form-label">Company Email</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$company->email}}"  name="title">
                        </div>


                    </div>  <div class="mws-form-row">
                        <label class="mws-form-label">Company Website</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$company->website}}"  name="title">
                        </div>
                    </div>

                </div>  <div class="mws-form-row">
                    <label class="mws-form-label">Company Image</label>
                    <div class="mws-form-item">
                        <img src="{{url('../../companyImages/',$company->image['image'])}}" width="200px" height="200px">
                    </div>
                </div>




                </div>

            </form>
        </div>
    </div>
@endsection

