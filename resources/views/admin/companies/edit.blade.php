
@extends('admin.master')
@section('content')

    @if (session('messge'))
        <div class="btn-success" style="height:15 px;width: 950px;font-size: 18px;"   >
            {{ session('messge') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <br>
    <div class="mws-panel grid_8">
        <div class="mws-panel-header">
            <span>Update Company</span>
        </div>
        <div class="mws-panel-body no-padding">
            <form class="mws-form" action="{{url('/company/update',$company->id)}}" method="POST" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="mws-form-inline">

                    <div class="mws-form-row">
                        <label class="mws-form-label">Company Name</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$company->name}}" name="name">
                        </div>
                    </div>
                    <div class="mws-form-row">
                        <label class="mws-form-label">Company Email</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$company->email}}"  name="email">
                        </div>


                    </div>  <div class="mws-form-row">
                        <label class="mws-form-label">Company Website</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$company->website}}"  name="website">
                        </div>
                    </div>

                    <div class="mws-form-row">
                        <label class="mws-form-label">Image</label>
                        <div class="mws-form-item">
                            <input type="file" class="small" name="image">
                        </div>
                    </div>

                </div>
                <div class="mws-button-row">
                    <input type="submit" value="Submit" class="btn btn-danger">
                    <input type="reset" value="Reset" class="btn ">
                </div>
            </form>
        </div>
    </div>
@endsection

