<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">

    <!-- Viewport Metatag -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <!-- Plugin Stylesheets first to ease overrides -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/plugins/colorpicker/colorpicker.css')}}" media="screen">

    <!-- Required Stylesheets -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/bootstrap/css/bootstrap.min.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/fonts/ptsans/stylesheet.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/fonts/icomoon/style.css')}}" media="screen">

    <link rel="stylesheet" type="text/css" href="{{url('assets/css/mws-style.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/icons/icol16.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/icons/icol32.css')}}" media="screen">

    <!-- Demo Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/demo.css')}}" media="screen">

    <!-- jQuery-UI Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/jui/css/jquery.ui.all.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{url('assets/jui/jquery-ui.custom.css')}}" media="screen">

    <!-- Theme Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/mws-theme.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/themer.css')}}" media="screen">

    <title>My Task</title>

</head>

<body>

<div id="mws-header" class="clearfix">



        <ul>

            <li><a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <p class="text-center">Log Out
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>


        <div id="mws-user-info" class="mws-inset">

        </div>
    </div>
</div>


<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <div id="mws-navigation">
            <ul>
                <li><a href="{{url('company')}}"><i class="icon-home"></i>Companies</a></li>

                <li>
                    <a href="{{url('employee')}}"><i class="icon-list"></i> Employees</a>

                </li>

            </ul>
        </div>

    </div>

    <!-- Main Container Start -->
    <div id="mws-container" >
        @yield('content')

        <div id="mws-footer"><br><br>

        </div>

    </div>
    <!-- Main Container End -->

</div>

<!-- JavaScript Plugins -->
<script src="{{url('assets/js/libs/jquery-1.8.3.min.js')}}"></script>
<script src="{{url('assets/js/libs/jquery.mousewheel.min.js')}}"></script>
<script src="{{url('assets/js/libs/jquery.placeholder.min.js')}}"></script>
<script src="{{url('assets/custom-plugins/fileinput.js')}}"></script>

<!-- jQuery-UI Dependent Scripts -->
<script src="{{url('assets/jui/js/jquery-ui-1.9.2.min.js')}}"></script>
<script src="{{url('assets/jui/jquery-ui.custom.min.js')}}"></script>
<script src="{{url('assets/jui/js/jquery.ui.touch-punch.js')}}"></script>

<!-- Plugin Scripts -->
<script src="{{url('assets/plugins/colorpicker/colorpicker-min.js')}}"></script>

<!-- Core Script -->
<script src="{{url('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/js/core/mws.js')}}"></script>



</body>
</html>