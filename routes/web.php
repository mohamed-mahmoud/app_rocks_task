<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/company');
});
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
       redirect('/company');
    });
    Route::group(['prefix' => 'company'], function () {
        Route::get('/', 'CompaniesController@index');
        Route::get('getEmployees/{id}', 'CompaniesController@get_employees');
        Route::get('edit/{id}', 'CompaniesController@edit');
        Route::get('show/{id}', 'CompaniesController@show');
        Route::get('insert', 'CompaniesController@insert');
        Route::get('delete/{id}', 'CompaniesController@delete');
        Route::post('store', 'CompaniesController@store');
        Route::post('update/{id}', 'CompaniesController@update');
    });


    Route::group(['prefix' => 'employee'], function () {
        Route::get('/', 'EmployeesController@index');
        Route::get('edit/{id}', 'EmployeesController@edit');
        Route::get('show/{id}', 'EmployeesController@show');
        Route::get('insert', 'EmployeesController@insert');
        Route::get('delete/{id}', 'EmployeesController@delete');
        Route::post('store', 'EmployeesController@store');
        Route::post('update/{id}', 'EmployeesController@update');
    });

});
Route::get('logout', 'LoginController@logout');
Auth::routes();

/*Route::get('/home', 'HomeController@index')->name('home');*/
