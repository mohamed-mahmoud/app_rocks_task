<?php

use Illuminate\Database\Seeder;
define('WP_MEMORY_LIMIT', '256m');
define('WP_MAX_MEMORY_LIMIT', '512m');
class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\Employee::class, 10)->create()->each(function ($s) {
//            $s->company()->saveMany(factory(\App\Company::class, 10)->create()->each(function ($u) {
//                $u->image()->save(factory(\App\Image::class)->make());
//            }));
////            factory(App\Company::class, 10)->create()->each(function($u){
////                $u->image()->save(factory(App\Image::class)->make());
////            });
//        });
        factory(App\Employee::class, 10)->create();
    }
}
