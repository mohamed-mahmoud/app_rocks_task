<?php

use Illuminate\Database\Seeder;
use \Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

//        Model::unguard();

        $this->call(CompaniesSeeder::class);
        $this->call(EmployeesSeeder::class);
        $this->call(ImageSeeder::class);

//        \Illuminate\Support\Facades\DB::table('users')->insert([
//            'name' => 'mohamed',
//            'email' => 'admin@company.com',
//            'password' => bcrypt('password'),
//        ]);
//
//        Model::reguard();
    }
}
